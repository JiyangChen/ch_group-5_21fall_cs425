package test.cs425.application.domain;

/**
 * @author JiyangChen
 *
 */
public class Document {
	private int docId;
	private String docName;
	private String title;
	
	public Document() {
	}
	public Document(int docId, String docName, String title) {
		this.docId = docId;
		this.docName = docName;
		this.title = title;
	}
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
}
