package test.cs425.application.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import test.cs425.application.domain.Document;
import test.cs425.application.service.DocumentService;

/**
 * @author JiyangChen
 *
 */
@Service
public class DocumentServiceImpl implements DocumentService{

	@Override
	public List<Document> findAll() {
		List<Document> list=new ArrayList<Document>();
		list.add(new Document(1101, "Computer Architecture", "Computer Architecture: A Quantitative Approach (The Morgan Kaufmann Series in Computer Architecture and Design)"));
		list.add(new Document(1102, "Digital Design", "Digital Design and Computer Architecture: ARM Edition"));
		list.add(new Document(1103, "Computer Science", "Computer Science Illuminated"));
		list.add(new Document(1104, "Computer Organization", "Computer Organization and Design RISC-V Edition: The Hardware Software Interface (The Morgan Kaufmann Series in Computer Architecture and Design)"));
		list.add(new Document(1105, "Study Guide for Fundamentals of Engineering", "Study Guide for Fundamentals of Engineering (FE) Electrical & Computer CBT Exam: Practice over 700 solved problems with detailed solutions based on NCEES® FE Reference Handbook Version 10.0.1"));
		return list;
	}

}
