package test.cs425.application.service;

import java.util.List;

import test.cs425.application.domain.Document;

/**
 * @author JiyangChen
 *
 */
public interface DocumentService {
	
	  public List<Document> findAll();

}
