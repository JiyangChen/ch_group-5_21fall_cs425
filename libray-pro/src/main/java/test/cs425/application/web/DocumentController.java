package test.cs425.application.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import test.cs425.application.domain.Document;
import test.cs425.application.service.DocumentService;

/**
 * @author JiyangChen
 *
 */
@Controller
@RequestMapping(value = "/documents")
public class DocumentController {
	
	private final Logger LOG = LoggerFactory.getLogger(DocumentController.class);


	@Autowired
    DocumentService documentService;

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public List<Document> getBookList() {
        return documentService.findAll();
    }
    
  
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Document getBook(@PathVariable Long id) {
		return null;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Void> postBook(@RequestBody Document doc, UriComponentsBuilder ucBuilder) {

        LOG.info("creating new document: {}", doc);

        if (doc.getDocName().equals("conflict")){
            LOG.info("a Document with name " + doc.getDocName() + " already exists");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        //documentService.insertDocument(doc);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/document/{id}").buildAndExpand(doc.getDocId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

   
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Document putBook(@RequestBody Document doc) {
		return doc;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public Document deleteBook(@PathVariable Long id) {
		return null;
    }


}
